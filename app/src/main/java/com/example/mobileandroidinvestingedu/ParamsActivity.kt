package com.example.mobileandroidinvestingedu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView

class ParamsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_params)
    }

    fun submitChanges(view: View) {
        val intent = Intent(this@ParamsActivity, MainActivity::class.java)
        val bodyKey = findViewById<TextView>(R.id.bodyKey).text.toString()
        val bodyValue = findViewById<TextView>(R.id.bodyValue).text.toString()
        val headerKey = findViewById<TextView>(R.id.headerKey).text.toString()
        val headerValue = findViewById<TextView>(R.id.headerValue).text.toString()
        val params = HashMap<String,String>()
        val headers = HashMap<String,String>()

        params[bodyKey] = bodyValue
        if (headerKey.isNotEmpty()) Log.e("key2 is not empty", headerKey)
        if (headerKey.isNotEmpty()) headers[headerKey] = headerValue
        intent.putExtra("params", params)
        intent.putExtra("headers", headers)

        startActivity(intent)
    }
}