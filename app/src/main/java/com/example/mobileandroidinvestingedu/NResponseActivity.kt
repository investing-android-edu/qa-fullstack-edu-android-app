package com.example.mobileandroidinvestingedu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.widget.TextView
import khttp.responses.Response

class NResponseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nresponse)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val url = intent.extras!!.getString("url")
        val params = intent.extras!!.getSerializable("params") as? HashMap<String, String>
        val headers = intent.extras!!.getSerializable("headers") as? HashMap<String, String>

        val response = sendRequest(url!!, params, headers)
        val message : String = response.text

        val responseText = findViewById<TextView>(R.id.responseText)
        responseText.text = message
    }

    private fun sendRequest(url:String, params:HashMap<String, String>?, headers:HashMap<String, String>?): Response {
        when (intent.extras!!.getString("requestType")) {
            "GET" -> {
                if (params == null) {
                    return if (headers == null) {
                        khttp.get(url)
                    } else {
                        khttp.get(
                            url = url,
                            headers = headers
                        )
                    }
                } else {
                    return if (headers == null) {
                        khttp.get(
                            url = url,
                            params = params)
                    } else {
                        khttp.get(
                            url = url,
                            headers = headers,
                            params = params
                        )
                    }
                }
            }
            else -> {
                if (params == null) {
                    return if (headers == null) {
                        khttp.post(url)
                    } else {
                        khttp.post(
                            url = url,
                            headers = headers
                        )
                    }
                } else {
                    return if (headers == null) {
                        khttp.post(
                            url = url,
                            json = params)
                    } else {
                        khttp.post(
                            url = url,
                            headers = headers,
                            json = params
                        )
                    }
                }
            }
        }
    }

    fun goBack(view: View) {
        val intent = Intent(this@NResponseActivity, MainActivity::class.java)
        startActivity(intent)
    }
}