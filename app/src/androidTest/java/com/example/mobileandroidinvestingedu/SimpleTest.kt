package com.example.mobileandroidinvestingedu

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.ActivityTestRule
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import org.junit.*

/**
 * Домашнее задание №3, №4. Информация в README.md файле
 */
class SimpleTest: BaseTest() {

    companion object {
        private const val BASE_URL = "http://localhost:8080"
    }

    var mockServer = WireMockServer(wireMockConfig().port(8080).bindAddress("localhost"))

    @Rule
    @JvmField
    var mActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Before
    fun initMockServer() {
        mockServer.start()
        wiremockRules()
        wiremockRulesHomework()
    }

    @After
    fun stopMockServer() {
        mockServer.stop()
    }

    @Test
    fun test1() {
        onView(withId(R.id.editText)).perform(clearText())
        onView(withId(R.id.editText)).perform(typeText("$BASE_URL/get"), closeSoftKeyboard())
        onView(withId(R.id.radioGet)).perform(click())
        onView(withId(R.id.button)).perform(click())

        onView(withId(R.id.responseText)).check(matches(withText("[Homework1]This is GET request!")))
    }

    @Test
    fun test2() {
        onView(withId(R.id.editParams)).perform(click())
        onView(withId(R.id.headerKey)).perform(clearText()).perform(typeText("param"))
        onView(withId(R.id.headerValue)).perform(clearText()).perform(typeText("value"))
        onView(withId(R.id.bodyKey)).perform(clearText())
        onView(withId(R.id.bodyValue)).perform(clearText())
        onView(withId(R.id.submitParams)).perform(click())

        onView(withId(R.id.editText)).perform(clearText())
        onView(withId(R.id.editText)).perform(typeText("$BASE_URL/get_with_headers"), closeSoftKeyboard())
        onView(withId(R.id.radioGet)).perform(click())
        onView(withId(R.id.button)).perform(click())

        onView(withId(R.id.responseText)).check(matches(withText("[Homework1]This is GET request with headers(param - value)!")))
    }

    @Test
    fun test3() {
        onView(withId(R.id.editParams)).perform(click())
        onView(withId(R.id.bodyKey)).perform(clearText()).perform(typeText("param"))
        onView(withId(R.id.bodyValue)).perform(clearText()).perform(typeText("value"))
        onView(withId(R.id.headerKey)).perform(clearText())
        onView(withId(R.id.headerValue)).perform(clearText())
        onView(withId(R.id.submitParams)).perform(click())

        onView(withId(R.id.editText)).perform(clearText())
        onView(withId(R.id.editText)).perform(typeText("$BASE_URL/post"), closeSoftKeyboard())
        onView(withId(R.id.radioGet)).perform(click())
        onView(withId(R.id.button)).perform(click())

        onView(withId(R.id.responseText)).check(matches(withText("[Homework1]This is POST request with body(param - value)!")))
    }

    @Test
    fun test4() {
        //TODO: Домашнее задание №4
    }

    @Test
    fun test5() {
        //TODO: Домашнее задание №4
    }

    @Test
    fun test6() {
        //TODO: Домашнее задание №4
    }

    @Test
    fun test4Kakao() {
        //TODO: Домашнее задание №5
    }

    @Test
    fun test5Kakao() {
        //TODO: Домашнее задание №5
    }

    @Test
    fun test6Kakao() {
        //TODO: Домашнее задание №5
    }

    @Test
    fun test4Kautomator() {
        //TODO: Домашнее задание №5
    }

    @Test
    fun test5Kautomator() {
        //TODO: Домашнее задание №5
    }

    @Test
    fun test6Kautomator() {
        //TODO: Домашнее задание №5
    }


    private fun wiremockRules() {
        stubFor(
            get(urlPathMatching("/get"))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("This is GET request"))
        )

        stubFor(
            get(urlPathMatching("/get"))
                .withHeader("key", matching("value"))
            .willReturn(aResponse()
                .withStatus(200)
                .withBody("This is GET request with header(key->value)"))
        )

        stubFor(
            post(urlPathMatching("/post"))
                .withHeader("key", matching("value"))
                .willReturn(aResponse()
                    .withStatus(200)
                    .withBody("This is POST request with body(key->value)"))
        )
    }

    private fun wiremockRulesHomework() {
        //TODO: Домашнее задание №3
    }
}